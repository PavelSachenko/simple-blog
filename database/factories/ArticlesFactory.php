<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->text(100),
        'text' => $faker->text(200),
        'likes' => 0,
        'watches' => 0,
        'user_id' => rand(1, 2),
        'category_id' => rand(1,3)
    ];
});
