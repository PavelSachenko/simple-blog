<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Admin', 'email' => 'a@gmail.com', 'password' => bcrypt('12345678'), 'is_admin' => 1],
            ['name' => 'Pavel', 'email' => 'p@gmail.com', 'password' => bcrypt('12345678'), 'is_admin' => 0],
            ['name' => 'rrr', 'email' => 'r@gmail.com', 'password' => bcrypt('12345678'), 'is_admin' => 0],
            ['name' => 'qqq', 'email' => 'q@gmail.com', 'password' => bcrypt('12345678'), 'is_admin' => 0],
        ]);
    }
}
