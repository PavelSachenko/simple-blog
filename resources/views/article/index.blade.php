@extends('patterns.index')

@section('title', 'Articles')
@section('content')

    <div class="row justify-content-between">
        @foreach($articles as $article)
            @include('patterns.cardarticle', $article)
        @endforeach

    </div>
    <div class="row">
        <div class="mt-3">
            {{$articles->links('vendor.pagination.bootstrap-4')}}
        </div>
    </div>

@endsection
