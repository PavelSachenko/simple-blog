@extends('patterns.index')

@section('title', 'Article')
@section('content')
    <div class="card">
        <img src="{{Storage::url($article->image)}}" style="width: 100%; height: 400px" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title text-center"><b>{{$article->title}}</b></h5>
            <hr>
            <p class="card-text">{{$article->description}}</p>
            <hr>
            <p class="card-text">{{$article->text}}</p>
        </div>
        <div class="mb-2">
            <div class="container mx-1">
                <div class="row justify-content-between">
                    <div class="text-left row ml-1">
                        <vue-like :route="{{json_encode(route('article.like', $article))}}"
                                  :article="{{json_encode($article)}}"
                                  :fill="'@fillattribute('Like', '$article->id') fa-heart'"></vue-like>
                        <vue-favorite :route="{{json_encode(route('article.favorite', $article))}}"
                                      :article="{{json_encode($article)}}"
                                      :fill="'@fillattribute('Favorite', '$article->id') fa-star'"></vue-favorite>
                        <div>
                            <span class="btn"><i class="far fa-eye"></i>{{$article->watches}}</span>
                        </div>
                    </div>
                    <div class="mr-5">
                        <small class="text-muted">{{$article->created_at}}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-3">
        <div class="row justify-content-between">
                <div class="col-md-6">
                    <div class="row">

                        <img src="{{Storage::url(\App\User::find($article->user_id)->image)}}" style="width: 50px; height: 50px">
                        <div class="ml-2">
                            <small class="text-muted">Created By</small>
                            <a href="{{route('users.solo', Auth::user())}}" class="d-block">{{\App\User::find($article->user_id)->name}}</a>
                        </div>
                    </div>
                </div>
                <div class="row col-md-3">
                    @creator($article->id)
                    <div>
                        <a class="mr-2 px-4 btn btn-secondary" href="{{route('article.edit', $article)}}">Edit</a>
                    </div>
                    <form action="{{route('article.destroy', $article)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-dark" href="{{route('article.destroy', $article)}}">Delete</button>
                    </form>
                    @endcreator

                </div>
            </div>
    </div>
    @include('patterns.comments.form', ['articleId' => $article->id, 'comments' => $comments])


@endsection
