@extends('patterns.index')

@section('title', 'Create Article')
@section('content')
    <form action="{{ (isset($article)) ? route('article.update', $article) : route('article.store') }}" method="POST" enctype="multipart/form-data">
        @if(isset($article))
            @method('PUT')
        @endif
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input name="title" type="text" class="form-control" id="title" aria-describedby="title" value="@if(isset($article)){{$article->title}}@endif{{old('title')}}">
            <small id="emailHelp" class="form-text text-muted">Title for article (view on card)</small>
            @include('patterns.error_message', ['param' => 'title'])
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" class="form-control" id="description" rows="3">@if(isset($article)){{$article->description}}@endif{{old('description')}}</textarea>
            <small id="emailHelp" class="form-text text-muted">Description for article (view on card)</small>
            @include('patterns.error_message', ['param' => 'description'])

        </div>
        <div class="form-group">
            <label for="category">Select category</label>
            <select name="category_id" class="form-control" id="category">
                @foreach(\App\Models\Article\Category::get() as $category)
                    <option value="{{$category->id}}">{{$category->title}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="text">Text</label>
            <textarea name="text" class="form-control" id="text" rows="10">@if(isset($article)){{$article->text}}@endif{{old('text')}}</textarea>
            <small id="emailHelp" class="form-text text-muted">Text for article</small>
            @include('patterns.error_message', ['param' => 'text'])
        </div>

        <div class="form-group">
            <label for="image">Image</label>
            <input name="image" type="file" class="form-control-file" id="image" value="{{old('image')}}">
            <small id="emailHelp" class="form-text text-muted">Select image for article (view on card)</small>
            @include('patterns.error_message', ['param' => 'image'])

        </div>
        @if(isset($article))
            <div class="form-group">
                <img src="{{Storage::url($article->image)}}" alt="..." style="width: 100%; height: 400px">
            </div>
        @endif
        <a class="btn btn-secondary" href="{{url()->previous()}}">Back</a>
        @if(isset($article))
            <button type="submit" class="btn btn-success" href="{{route('article.update', $article)}}">Update Article</button>
        @else
            <button type="submit" class="btn btn-success" href="{{route('article.store')}}">Create Article</button>
        @endif
    </form>
@endsection
