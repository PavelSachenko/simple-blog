@extends('patterns.index')
@section('title', 'Launch')
@section('launch')
    <div class="alert alert-primary" role="alert">
        <p>For login</p>
        <p>Admin: a@gmail.com - password: 12345678</p>
        <p>User1: p@gmail.com - password: 12345678</p>
        <p>User2: r@gmail.com - password: 12345678</p>
        <p>User3: q@gmail.com - password: 12345678</p>
    </div>
    <div class="alert alert-success" role="alert">
        <a href="{{route('reset')}}" class="btn btn-success">Reset Project</a>
    </div>
@endsection
