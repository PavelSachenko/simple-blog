<div class="container my-5">
    <div class="card">
        <div class="card-header bg-dark">
            <small class="text-muted">Comments:</small>
        </div>
        <div class="card-body ml-2">
            <form action="{{route('article.comment', $articleId)}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="comment"></label>
                    <textarea name="comment" class="form-control" id="comment" rows="3" placeholder="Write your comment"></textarea>
                </div>
                <button type="submit" class="btn btn-secondary">Comment</button>
            </form>
            @foreach($comments as $comment)
                <hr>
                    @include('patterns.comments.comment')
                <hr>
            @endforeach

        </div>
    </div>
</div>
