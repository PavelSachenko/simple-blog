<div class="container">

    <div class="row">
        <div class="">
            <div class="row">
                <img src="{{Storage::url(\App\User::find($comment->user_id)->image)}}" style="height: 50px; width: 50px;">
                <div class="flex-column ml-2 mt-3">
                    <small class="d-block text-muted">{{$comment->updated_at}}</small>
                    <a href="{{route('users.solo', Auth::user())}}" class="">{{\App\User::find($comment->user_id)->name}} </a>
                </div>
            </div>
        </div>
        <div class="alert alert-secondary col-md-9 ml-4">
            <p>{{$comment->text}}</p>
        </div>

    </div>
</div>
