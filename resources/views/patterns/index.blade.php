<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" />
    <title>@yield('title')</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link @active('index')" href="{{route('index')}}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @active('article.index')" href="{{route('article.index')}}">Articles</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @active('article.popular')" href="{{route('article.popular')}}">Popular Article</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @active('article.create')" href="{{route('article.create')}}">Create Article</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @active('users.index')" href="{{route('users.index')}}">Users</a>
                </li>
                @admin
                <li class="nav-item">
                    <a class="nav-link @active('admin.index')" href="{{route('admin.index')}}">Admin Panel</a>
                </li>
                @endadmin

            </ul>
        </div>
        <div class="flex-center position-ref full-height">
            <ul class="navbar-nav">
            @if (Route::has('login'))
                <div class="top-right links row">
                    @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{Auth::user()->name}}
                            </a>
                            <div class="dropdown-menu bg-secondary" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{route('article.user', Auth::user()->id)}}">My Articles</a>
                                <a class="dropdown-item" href="{{route('article.favorites')}}">Favorites</a>
                                <a class="dropdown-item" href="{{route('article.likes')}}">Likes</a>
                                <a class="dropdown-item" href="{{route('user.followers')}}">Followers</a>
                                <a class="dropdown-item" href="{{route('user.following')}}">Following</a>
                                <a class="dropdown-item" href="{{route('user.settings')}}">Settings</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @else

                        <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Login</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">

                            <a class="nav-link" href="{{ route('register') }}">Register</a>
                            </li>
                        @endif
                    @endauth
                </div>
            @endif
            </ul>
        </div>
    </div>
</nav>
<div id="app">
    <div class="container mt-5">
        <div class="row justify-content-between">
            <div class="col-md-9">
                @yield('content')
                @yield('launch')
            </div>
            @auth
            <div class="col-md-3">
                <div class="row justify-content-end">
                    <div class="col-md-10">
                        <div class="card" style="width: 18rem;">
                            <div class="card-header">
                                Categories
                            </div>
                            <ul class="list-group list-group-flush">
                                @foreach(\App\Models\Article\Category::get() as $category)
                                    <a href="{{route('category.index', $category->id)}}"><li class="list-group-item">{{$category->title}}</li></a>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @endauth

        </div>
    </div>
</div>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
