<div class="card mb-3 mx-3" style="width: 250px;">
    <img src="{{Storage::url($user->image)}}" style="height: 250px; width: 250px">
    <div class="card-body">
        <h5 class="card-title">{{$user->name}}</h5>
        <div class="alert alert-secondary">
            <p class="card-text" style="height: 100px">{{$user->description}}</p>
        </div>
        <div class="container ">
        <div class="row justify-content-between">
        @auth
        <form action="{{route('user.follow', $user->id)}}" method="POST">
            @csrf
            @follow($user->id)
                <button type="submit" class="btn btn-secondary">unfollow</button>
            @else
                <button type="submit" class="btn btn-primary">follow</button>
            @endfollow
        </form>
        @endauth
            <a href="{{route('article.user', $user->id)}}" class="btn btn-success">All article</a>
        </div>
        </div>
    </div>
</div>
