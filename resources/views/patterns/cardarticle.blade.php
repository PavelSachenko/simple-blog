<div class="card mb-4" style="width: 25rem">
    <img src="{{\Illuminate\Support\Facades\Storage::url($article->image)}}" class="card-img-top" style="width: 100%; height: 300px;"  alt="...">
    <div class="card-body">
        <h5 class="card-title text-center solid"><b>{{$article->title}}</b></h5>
        <hr>
        <p class="card-text" style="height: 100px">{{$article->description}}</p>
        <hr>

        <div class="row justify-content-between">
            <div class="text-left row ml-1">
                <vue-like :route="{{json_encode(route('article.like', $article))}}"
                          :article="{{json_encode($article)}}"
                          :fill="'@fillattribute('Like', '$article->id') fa-heart'"></vue-like>
                <vue-favorite :route="{{json_encode(route('article.favorite', $article))}}"
                              :article="{{json_encode($article)}}"
                              :fill="'@fillattribute('Favorite', '$article->id') fa-star'"></vue-favorite>
                <div>
                    <span class="btn"><i class="far fa-eye"></i>{{$article->watches}}</span>
                </div>
            </div>
            <div>
                <a href="{{route('article.show', $article)}}" class="btn btn-primary mr-3">show</a>
            </div>
        </div>
    </div>
</div>




