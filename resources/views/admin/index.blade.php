@extends('patterns.index')

@section('title', 'Admin Panel')

@section('content')
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Articles</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Categories</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Users</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">@include('admin.articles.index', $articles)</div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">@include('admin.categories.index', $categories)</div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">@include('admin.users.index', $categories)</div>
    </div>
@endsection
