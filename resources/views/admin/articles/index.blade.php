<div class="container">
    @foreach($articles as $article)
        <hr>
        <div class="row mt-2">
            <div class="col-md-6 d-flex align-items-center">
                <span><b>Title: </b></span><span class="ml-2"> {{$article->title}}</span>
                <span class="ml-2"><b>Created:</b></span><span class="ml-2"> {{$article->created_at}}</span>
            </div>
            <div class="col-md-6">
                <div class="row d-flex align-items-center justify-content-end">
                    <i class="fas fa-heart">{{$article->likes}}</i>
                    <i class="fas fa-eye ml-2 mr-2">{{$article->watches}}</i>
                    <form action="{{route('article.destroy', $article)}}" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-dark mx-1">Delete</button>
                    </form>
                </div>
            </div>
        </div>
        <hr>
    @endforeach
    {{$articles->links('vendor.pagination.bootstrap-4')}}
</div>
