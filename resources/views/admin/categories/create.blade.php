@extends('patterns.index')
@section('title', 'Category')
@section('content')
    <form action="{{(isset($category)) ? route('admin.category.update', $category) : route('admin.category.store')}}" method="POST">
        @csrf
        @if(isset($category))
        @method('PUT')
        @endif
        <div class="form-group">
            <label>Category title:</label>
            <input name="title" type="text" class="form-control" value="@if(isset($category)){{$category->title}}@endif"/>
        </div>
        @include('patterns.error_message', ['param' => 'title'])
        <button type="submit" class="btn btn-dark">Send</button>
    </form>
@endsection
