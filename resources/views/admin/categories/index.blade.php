<div class="container">
    <div class="row justify-content-end">
        <a class="btn btn-info mt-2" href="{{route('admin.category.create')}}">Add Category</a>
    </div>
    @foreach($categories as $category)
    <hr>
    <div class="row mt-2">
        <div class="col-md-6 d-flex align-items-center">
            <span><b>Title: </b></span><span class="ml-2"> {{$category->title}}</span>
        </div>
        <div class="col-md-6">
            <div class="row justify-content-end">
                <a href="{{route('admin.category.edit', $category)}}" class="btn btn-secondary mx-1">Edit</a>
                <form action="{{route('admin.category.delete', $category)}}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-dark mx-1">Delete</button>
                </form>
            </div>
        </div>
    </div>
    <hr>
    @endforeach
    {{$categories->links('vendor.pagination.bootstrap-4')}}
</div>
