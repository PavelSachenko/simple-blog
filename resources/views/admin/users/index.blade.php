<div class="container">
    @foreach($users as $user)
        <hr>
        <div class="row mt-2">
            <div class="col-md-6 d-flex align-items-center">
                <span><b>Name: </b></span><span class="ml-2"> {{$user->name}}</span>
                <span class="ml-2"><b>Admin:</b></span><span class="ml-2">@if($user->is_admin) YES @else NO @endif</span>
            </div>
            <div class="col-md-6">
                <div class="row d-flex align-items-center justify-content-end">
                    <form action="{{route('admin.up', $user)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="btn btn-secondary mx-1">UP Access</button>
                    </form>
                    <form action="{{route('admin.user.delete', $user)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-dark mx-1">Delete</button>
                    </form>
                </div>
            </div>
        </div>
        <hr>
    @endforeach
    {{$users->links('vendor.pagination.bootstrap-4')}}
</div>
