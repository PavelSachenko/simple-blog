@extends('patterns.index')
@section('title', 'User ' . $user->name)
@section('content')
    <div class="row justify-content-start">
        @include('patterns.carduser', $user)
    </div>
@endsection
