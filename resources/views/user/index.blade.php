@extends('patterns.index')
@section('title', 'Users')

@section('content')
    <div class="row justify-content-start">
        @foreach($users as $user)
            @include('patterns.carduser', $user)
        @endforeach
    </div>
@endsection
