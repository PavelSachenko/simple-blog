@extends('patterns.index')

@section('title', 'User settings')

@section('content')
    <div class="card" >
        <div class="card-body bg-white">
            <small class="card-subtitle mb-2 text-muted">Setting profile:</small>
            <form action="{{route('user.update')}}" method="POST" class="mt-2" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <div class="d-flex align-items-end">
                        <img src="{{Storage::url(Auth::user()->image)}}" style="height: 250px; width: 250px">
                        <img src="{{Storage::url(Auth::user()->image)}}" class="ml-3" style="height: 50px; width: 50px">
                    </div>
                    <input name="image" type="file" class="form-control-file mt-2">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input name="description" type="text" class="form-control" id="description" value="{{Auth::user()->description}}">
                    <small class="form-text text-muted">Description</small>
                </div>
                <div class="form-group">
                    <label for="name">Nick-Name</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{Auth::user()->name}}">
                    <small  class="form-text text-muted">Nick-Name</small>
                </div>
                @include('patterns.error_message', ['param' => 'name'])
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
