<?php

namespace App\Models\Article;

use App\Scopes\ArticleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Favorite extends Model
{
    use ArticleScope;
    protected $guarded = [];

    public static function addFavorite($article){
        $query = self::isBind($article->id);
        if($query){
            if($query->first() == null){
                self::create(['article_id' => $article->id, 'user_id' => Auth::user()->id]);
            }else{
                $query->first()->delete();
            }
            return true;
        }
        return false;

    }
}
