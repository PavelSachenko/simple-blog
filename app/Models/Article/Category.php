<?php

namespace App\Models\Article;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    protected $table = 'categories';

    public function articles(){
        return $this->hasMany(Article::class, 'category_id');
    }
}
