<?php

namespace App\Models\Article;

use App\Models\Article;
use App\Scopes\ArticleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Watch extends Model
{
    use ArticleScope;
    protected $guarded = [];

    public static function addWatch(Article $article){
        $query = self::isBind($article->id);
        if ($query){
            if ($query->first() == null) {
                $article->update(['watches' => $article->watches += 1]);
                self::create(['article_id' => $article->id, 'user_id' => Auth::user()->id]);
            }
            return true;
        }
        return false;
    }
}
