<?php

namespace App\Models\Article;

use App\Models\Article;
use App\Scopes\ArticleScope;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Like extends Model
{
    use ArticleScope;
    protected $guarded = [];

    public function posts(){
        return $this->belongsTo(Article::class, 'article_id');
    }

    public static function putLike($article){
        $query = self::isBind($article->id);
        if($query){
            if ($query->first() == null) {
                $article->update(['likes' => $article->likes += 1]);
                self::create(['article_id' => $article->id, 'user_id' => Auth::user()->id]);
            }else{
                $article->update(['likes' => $article->likes -= 1]);
                self::isBind($article->id)->first()->delete();
            }
        }
        return false;
    }
}
