<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Sub extends Model
{
    protected $table = 'following';
    protected $guarded = [];
    public function scopeIsFollow($query, $subId){
        return $query->where([['user_id', Auth::user()->id], ['sub', $subId]]);
    }
    public static function follow($userId){
        if(self::isFollow($userId)->first() == null){
            self::create(['user_id' => Auth::user()->id, 'sub' => $userId]);
        }else{
            self::isFollow($userId)->first()->delete();
        }
    }
}
