<?php

namespace App\Models;

use App\Models\Article\Comment;
use App\Models\Article\Favorite;
use App\Models\Article\Like;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Article extends Model
{
    protected $guarded = [];

    public function comments(){
        return $this->hasMany(Comment::class, 'article_id');
    }
    public function likes(){
        return $this->hasMany(Like::class, 'article_id');
    }
    public function favorites(){
        return $this->hasMany(Favorite::class, 'article_id');
    }

}
