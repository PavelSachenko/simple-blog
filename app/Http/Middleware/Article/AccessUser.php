<?php

namespace App\Http\Middleware\Article;

use App\Models\Article;
use Closure;
use Illuminate\Support\Facades\Auth;

class AccessUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Article::where([['user_id', Auth::user()->id], ['id', $request->route('article')->id]])->first() && !Auth::user()->is_admin)
            dd('asd');
        return $next($request);
    }
}
