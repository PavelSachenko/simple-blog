<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    public function index(){
        return view('user.settings.index');
    }

    public function update(Request $request){

        $request->validate([
           'name' => ['required','min:2','max:15']
        ]);

        $params = $request->all();
        $user = User::find(Auth::user()->id);
        if($request->has('image')){
            if($user->image != null)
                Storage::delete($user->image);
            $params['image'] = $request->file('image')->store('public/user');
        }
        $user->update($params);
        return redirect()->route('user.settings');
    }
}
