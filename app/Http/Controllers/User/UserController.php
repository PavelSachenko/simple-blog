<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Article\Comment;
use App\Models\Article\Favorite;
use App\Models\User\Sub;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(){
        if(Auth::user())
            $users = User::where('id', '!=', Auth::user()->id)->get();
        else
            $users = User::get();
        return view('user.index', compact('users'));
    }

    public function follow($userId){
        Sub::follow($userId);
        return redirect()->back();
    }

    public function following(){
        $users = User::find(Auth::user()->id)->following()->get();
        return view('user.index', compact('users'));
    }

    public function followers(){
        $users = User::find(Auth::user()->id)->followers()->get();
        return view('user.index', compact('users'));
    }

    public function solo(User $user){
        return view('user.solo', compact('user'));
    }




}
