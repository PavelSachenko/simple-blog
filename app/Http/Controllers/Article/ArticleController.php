<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleCheck;
use App\Models\Article;
use App\Models\Article\Category;
use App\Models\Article\Comment;
use App\Models\Article\Watch;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    public function index(){
        $articles = Article::orderBy('created_at', 'DESC')->paginate(4);
        return view('article.index', compact('articles'));
    }

    public function popular(){
        $articles = Article::orderBy('likes', 'DESC')->paginate(4);
        return view('article.index', compact('articles'));
    }

    public function create(){
        return view('article.create');
    }

    public function store(ArticleCheck $request){
        $params = $request->all();
        if($request->has('image')){
            $params['image'] = $request->file('image')->store('public/article');
        }
        $params['user_id'] = Auth::user()->id;
        $article = Article::create($params);
        return redirect()->route('article.show', compact('article'));
    }

    public function show(Article $article){
        Watch::addWatch($article);
        $comments = Article::find($article->id)->comments()->get();
        return view('article.show', compact('article', 'comments'));
    }

    public function edit(Article $article){
        return view('article.create', compact('article'));
    }

    public function update(ArticleCheck $request, Article $article){
        $params = $request->all();
        if($request->has('image')){
            Storage::delete($article->image);
            $params['image'] = $request->file('image')->store('public/article');
        }
        $article->update($params);
        return redirect()->route('article.show', $article);
    }

    public function destroy(Article $article){
        $article->delete();
        return redirect()->back();
    }

    public function like($articleId){
        $article = Article::find($articleId);
        $article->update(['likes' => $article->likes+1]);
        return json_encode(['likes' => $article->likes]);
    }

    public function showUserArticle($userId){
        $articles = User::find($userId)->articles()->paginate(4);
        return view('article.index', compact('articles'));
    }

    public function comment(Request $request, $articleId){
        $data['text'] = $request->comment;
        $data['article_id'] = $articleId;
        $data['user_id'] = Auth::user()->id;
        Comment::create($data);
        return redirect()->back();
    }

}
