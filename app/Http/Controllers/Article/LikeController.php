<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Article\Like;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function like(Article $article){
        if(Like::putLike($article));
            return json_encode(['likes' => $article->likes]);
        return json_encode(['likes' => 'error']);
    }

    public function show(){
        $articles = User::find(Auth::user()->id)->likes()->paginate(4);
        return view('article.index', compact('articles'));
    }

}
