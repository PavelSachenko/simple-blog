<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Article\Favorite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function favorite(Article $article){
        Favorite::addFavorite($article);
        return json_encode(['likes' => $article->likes]);
    }
    public function show(){
        $articles = User::find(Auth::user()->id)->favorites()->paginate(4);
        return view('article.index', compact('articles'));
    }
}
