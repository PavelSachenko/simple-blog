<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Models\Article\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index($categoryId){
        $articles = Category::find($categoryId)->articles()->paginate(4);
        return view('article.index', compact('articles'));
    }
}
