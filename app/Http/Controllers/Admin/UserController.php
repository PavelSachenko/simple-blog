<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function up(User $user){
        $user->update(['is_admin' => 1]);
        return redirect()->back();
    }

    public function destroy(User $user){
        $user->delete();
        return redirect()->back();
    }
}
