<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Article\Category;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        $categories = Category::paginate(5);
        $articles = Article::paginate(5);
        $users = User::paginate(5);
        return view('admin.index', compact('categories', 'articles', 'users'));
    }


}
