<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryCheck;
use App\Models\Article\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function create(){
        return view('admin.categories.create');
    }

    public function store(CategoryCheck $request){
        Category::create($request->all());
        return redirect()->route('admin.index');
    }

    public function edit(Category $category){
        return view('admin.categories.create', compact('category'));
    }

    public function update(CategoryCheck $request,Category $category){
        $category->update(['title' => $request->title]);
        return redirect()->route('admin.index');
    }

    public function destroy(Category $category){
        $category->delete();
        return redirect()->back();
    }
}
