<?php


namespace App\Scopes;

use App\Models\Article;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

trait ArticleScope
{
    public static function scopeIsBind($query, $articleId)
    {
        if(isset(Auth::user()->id))
            return $query->where([['user_id', Auth::user()->id], ['article_id', $articleId]]);
        else
            return false;
    }


}
