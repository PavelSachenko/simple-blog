<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Like;
use App\Scopes\ArticleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use App\Models\Watch;
class AppServiceProvider extends ServiceProvider
{
    use ArticleScope;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::directive('active', function ($routeName){
            return "<?php echo (Request::route()->named($routeName) ? 'active' : ''); ?>";
        });
        //old version - use without ajax for refresh (likes, favorites)
        Blade::directive('fillattribute', function ($data){
            eval("\$params = [$data];");
            list($model, $articleId) = $params;
            return "<?php if(App\Models\Article\\$model::isBind($articleId)){ echo (App\Models\Article\\$model::isBind($articleId)->first() == null ) ? 'far' : 'fas';}else{echo 'far';} ?>";
        });

        Blade::if('follow', function ($userId){
            return \App\Models\User\Sub::where([['user_id', Auth::user()->id], ['sub', $userId]])->first();
        });

        Blade::if('creator', function ($articleId){
            return Article::where([['user_id', Auth::user()->id], ['id', $articleId]])->first();
        });

        Blade::if('admin', function (){
            if (Auth::user() && Auth::user()->is_admin)
                return true;
            return false;
        });
    }
}
