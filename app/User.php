<?php

namespace App;

use App\Models\Article;
use App\Models\Article\Favorite;
use App\Models\Article\Like;
use App\Models\User\Follower;
use App\Models\User\Sub;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public function articles(){
        return $this->hasMany(Article::class, 'user_id');
    }

    public function likes(){
        return $this->belongsToMany(Article::class, 'likes');
    }


    public function favorites(){
        return $this->belongsToMany(Article::class, 'favorites');
    }


    public function following(){
        return $this->belongsToMany(User::class, 'following', 'user_id', 'sub');
    }
    public function followers(){
        return $this->belongsToMany(User::class, 'following', 'sub', 'user_id');
    }



    public function scopeAdmin($query){
        return $query->where('is_admin', 1);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'description', 'image', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
