## Set-up Project
- run composer install to generate depedencies in vendor folder
- change .env.example to .env (open .env and change configuration on yours ("DB_DATABASE", "DB_USERNAME", "DB_PASSWORD"))
- run php artisan key:generate
- run SQL server
- run php artisan serve
- follow the link "/" on your site
- Click the button "Reset project"
- Complete !!!

