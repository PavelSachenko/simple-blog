<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('patterns.instruction');
})->name('index');

Route::get('/reset', function (){
    Artisan::call('migrate:fresh --seed');
    Storage::deleteDirectory('public');
    return redirect()->route('index');
})->name('reset');

Auth::routes();




Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function (){
    Route::get('/', 'Admin\AdminController@index')->name('admin.index');
    Route::put('/up/{user}', 'Admin\UserController@up')->name('admin.up');
    Route::delete('/user/delete/{user}',  'Admin\UserController@destroy')->name('admin.user.delete');

    Route::get('/category/create', 'Admin\CategoryController@create')->name('admin.category.create');
    Route::get('/category/{category}/edit', 'Admin\CategoryController@edit')->name('admin.category.edit');
    Route::post('/category/store', 'Admin\CategoryController@store')->name('admin.category.store');
    Route::put('/category/{category}/update/', 'Admin\CategoryController@update')->name('admin.category.update');
    Route::delete('/category/{category}/delete/', 'Admin\CategoryController@destroy')->name('admin.category.delete');


});

Route::group(['prefix' => 'articles'], function (){

    Route::group(['middleware' => 'auth'], function (){
        Route::get('/create', 'Article\ArticleController@create')->name('article.create');
        Route::post('/store', 'Article\ArticleController@store')->name('article.store');

        Route::group(['middleware' => 'access'], function (){
            Route::get('/{article}/edit', 'Article\ArticleController@edit')->name('article.edit');
            Route::put('/{article}/update', 'Article\ArticleController@update')->name('article.update');
            Route::delete('/{article}/destroy', 'Article\ArticleController@destroy')->name('article.destroy');
        });

        Route::post('/{article}/like', 'Article\LikeController@like')->name('article.like');
        Route::post('/{article}/favorite', 'Article\FavoriteController@favorite')->name('article.favorite');

        Route::get('/likes', 'Article\LikeController@show')->name('article.likes');
        Route::get('/favorites', 'Article\FavoriteController@show')->name('article.favorites');

        Route::post('/{articleId}/comment', 'Article\ArticleController@comment')->name('article.comment');
    });

    Route::get('/', 'Article\ArticleController@index')->name('article.index');
    Route::get('/popular', 'Article\ArticleController@popular')->name('article.popular');
    Route::get('/{article}', 'Article\ArticleController@show')->name('article.show');
    Route::get('/user/{user_id}', 'Article\ArticleController@showUserArticle')->name('article.user');

    Route::get('category/{categoryId}', 'Article\CategoryController@index')->name('category.index');

});

Route::group(['prefix' => 'user'], function (){

    Route::get('/all', 'User\UserController@index')->name('users.index');
    Route::group(['middleware' => 'auth'], function (){
        Route::get('/settings', 'User\SettingController@index')->name('user.settings');
        Route::put('/update', 'User\SettingController@update')->name('user.update');

        Route::post('/follow/{userId}', 'User\UserController@follow')->name('user.follow');
        Route::get('/following', 'User\UserController@following')->name('user.following');
        Route::get('/followers', 'User\UserController@followers')->name('user.followers');
    });

    Route::get('/{user}', 'User\UserController@solo')->name('users.solo');

});


Route::get('/home', 'HomeController@index')->name('home');
